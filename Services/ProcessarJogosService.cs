﻿using System;
using System.Collections.Generic;
using System.Linq;
using squadra.Data.Models;
using squadra.Util;

namespace squadra.Services
{
    public class ProcessarJogosService
    {
        private List<Equipe> _equipes;
        private List<Jogo> _jogos;

        public ProcessarJogosService(List<Equipe> equipes)
        {
            _equipes = equipes;
            _jogos = new List<Jogo>();
        }

        /**
         * Trabalha a fila de Equipes para gerar os jogos
         */
        public void gerarJogos()
        {
            // O campeonato são 03 rodadas no total com
            // 07 jogos. A primeira rodada são 04 jogos.
            List<Equipe> jogadoresRodadaUm = _equipes;
            List<Equipe> jogadoresRodadaDois = new List<Equipe>();
            List<Equipe> jogadoresRodadaTres = new List<Equipe>();

            for (int rodadas = 0; rodadas <= 3; rodadas++)
            {
                var equipeFirst = jogadoresRodadaUm.First();
                var equipeLast = jogadoresRodadaUm.Last();

                jogadoresRodadaUm.Remove(equipeFirst);
                jogadoresRodadaUm.Remove(equipeLast);

                Equipe equipeVencedora = EquipeVencedora.Vencedor(equipeFirst, equipeLast);

                // Salvar o jogo
                _jogos.Add(new Jogo
                {
                    EquipeFirst = equipeFirst.Nome,
                    EquipeLast = equipeLast.Nome,
                    EquipeVencedora = equipeVencedora.Nome,
                    Fase = 0
                }); ;

                jogadoresRodadaDois.Add(equipeVencedora);
            }

            // Começar agora a segunda rodada
            for (int rodada = 0; rodada <= 1; rodada++)
            {
                var equipeFirst = jogadoresRodadaDois.First();
                jogadoresRodadaDois.Remove(equipeFirst);

                var equipeLast = jogadoresRodadaDois.First();
                jogadoresRodadaDois.Remove(equipeLast);

                Equipe equipeVencedora = EquipeVencedora.Vencedor(equipeFirst, equipeLast);

                // Salvar o jogo
                _jogos.Add(new Jogo
                {
                    EquipeFirst = equipeFirst.Nome,
                    EquipeLast = equipeLast.Nome,
                    EquipeVencedora = equipeVencedora.Nome,
                    Fase = 1
                });

                jogadoresRodadaTres.Add(equipeVencedora);
            }

            // Última Rodada/Jogo agora
            var equipeSemiFirst = jogadoresRodadaTres.First();
            jogadoresRodadaTres.Remove(equipeSemiFirst);

            var equipeSemiLast = jogadoresRodadaTres.First();
            jogadoresRodadaTres.Remove(equipeSemiLast);

            Equipe campeao = EquipeVencedora.Vencedor(equipeSemiFirst, equipeSemiLast);

            // Salvar o jogo
            _jogos.Add(new Jogo
            {
                EquipeFirst = equipeSemiFirst.Nome,
                EquipeLast = equipeSemiLast.Nome,
                EquipeVencedora = campeao.Nome,
                Fase = 2
            });
        }

        /**
         * Retorna a lista de Jogos
         */
        public List<Jogo> resultado()
        {
            return _jogos;
        }
    }
}
