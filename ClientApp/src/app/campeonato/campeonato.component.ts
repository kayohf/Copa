import { Component, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { Campeonato } from './campeonato';

@Component({
  selector: 'app-campeonato',
  templateUrl: './campeonato.component.html'
})

export class CampeonatoComponent {
  public loading: Boolean = true;

  public campeonatos: Campeonato[];

  constructor(
    private http: HttpClient,
    @Inject('BASE_URL') private baseUrl: string
  ) { }

  ngOnInit() {
    this.http.get<Campeonato[]>(this.baseUrl + 'api/Campeonatos').subscribe(resultado => {
      this.campeonatos = resultado;
      this.loading = false;
    }, error => console.error(error))
  }
}