export interface Campeonato {
  id: string;
  nome: string;
  ativo: string;
  createdAt: Date;
}