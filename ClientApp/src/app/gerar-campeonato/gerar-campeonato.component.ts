import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Equipe } from '../equipes/equipe';

@Component({
  selector: 'app-gerar-campeonato',
  templateUrl: './gerar-campeonato.component.html'
})

export class GerarCampeonatoComponent {
  public loading: Boolean = true;

  public goodToGo: Boolean = false;
  public equipeIds: string[] = [];

  public nomeCampeonato: string;
  public campeonatoId: string;

  public sucesso: boolean;
  public resultadoMsg: string;

  public equipes: Equipe[];

  constructor(
    private http: HttpClient,
    @Inject('BASE_URL') private baseUrl: string
  ) { }

  ngOnInit() {
    this.http.get<Equipe[]>(this.baseUrl + 'api/Equipes').subscribe(resultado => {
      this.equipes = resultado;
      this.loading = false;
    }, error => console.error(error))
  }

  // Recebe o evento do toggle e adicionar/remove o ID da Equipe

  adicionarEquipe(evento, id) {
    if (evento.checked == true) {
      this.equipeIds.push(id);
    } else {
      // Remove do array
      let index = this.equipeIds.indexOf(id);

      if (index > -1) {
        this.equipeIds.splice(index, 1);
      }
    }

    if (this.equipeIds.length == 8) {
      this.goodToGo = true;

      return;
    }
  }

  enviarEquipes() {
    this.loading = true;

    let formData = new FormData();
    formData.append('nome', this.nomeCampeonato);
    formData.append('selecao', this.equipeIds.toString());

    this.http.post<any>(this.baseUrl + 'api/Campeonatos', formData).subscribe(retorno => {

      this.sucesso = retorno.sucesso;
      this.resultadoMsg = retorno.mensagem;
      this.loading = false;
      this.campeonatoId = retorno.campeonatoId;

    }, error => console.error(error))
  }
}