import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './angular-material.module';
import { GerarCampeonatoComponent } from './gerar-campeonato/gerar-campeonato.component';
import { CampeonatoComponent } from './campeonato/campeonato.component';
import { VerCampeonatoComponent } from './ver-campeonato/ver-campeonato.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    GerarCampeonatoComponent,
    CampeonatoComponent,
    VerCampeonatoComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'gerar-campeonato', component: GerarCampeonatoComponent },
      { path: 'campeonato', component: CampeonatoComponent },
      { path: 'ver-campeonato/:id', component: VerCampeonatoComponent }
    ]),
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
