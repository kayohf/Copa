export interface Jogo {
  jogoId: string;
  campeonatoId: string;
  equipeFirst: string;
  equipeLast: string;
  equipeVencedora: string;
  fase: number;
}