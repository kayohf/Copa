import { Component, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

import { Campeonato } from '../campeonato/campeonato';
import { Jogo } from './jogo';

@Component({
  selector: 'app-ver-campeonato',
  templateUrl: './ver-campeonato.component.html'
})

export class VerCampeonatoComponent {
  public id: string;

  public loading: Boolean = true;

  public campeonato: Campeonato;
  public jogos: Jogo[];

  public jogoFinal: any;
  public segundoLugar: string;

  constructor(
    public route: ActivatedRoute,
    private http: HttpClient,
    @Inject('BASE_URL') private baseUrl: string
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
    });

    this.http.get<Campeonato>(this.baseUrl + 'api/Campeonatos/' + this.id).subscribe(resultado => {
      this.campeonato = resultado;
    }, error => console.error(error))

    this.http.get<Jogo[]>(this.baseUrl + 'api/Campeonatos/' + this.id + '/Jogos').subscribe(resultado => {
      this.jogos = resultado;
      this.loading = false;

      this.jogoFinal = this.jogos.find(j => j.fase == 2);

      // Para achar o perdedor do jogo final, tenho que fazer
      // esse pequeno hack, nao estou feliz com isso.
      let hack: string[] = [];

      hack.push(this.jogoFinal.equipeFirst);
      hack.push(this.jogoFinal.equipeLast);

      this.segundoLugar = hack.find(equipe => equipe != this.jogoFinal.equipeVencedora);
    })
  }
}