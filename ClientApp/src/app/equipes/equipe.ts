export interface Equipe {
  equipeId: string;
  nome: string;
  sigla: string;
  gols: number;
}