﻿## Sobre o projeto

- **Backend** ASPNet Core / EFCore
- **Frontend** Angular 8
- **Banco** SQL Server no Azure

## Instalação

1. Configure a string de configuração do seu banco em appsetings.json
2. Rode 'dotnet ef database update' para sincronizar as tabelas e executar o Seed
3. Rode 'dotnet run' para executar