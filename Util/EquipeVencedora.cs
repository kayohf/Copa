﻿using System;
using squadra.Data.Models;

namespace squadra.Util
{
    public static class EquipeVencedora
    {
        public static Equipe Vencedor(Equipe equipeFirst, Equipe equipeLast)
        {
            Equipe equipeVencedora;

            // Vamos agora verificar quem ganha
            if (equipeFirst.Gols > equipeLast.Gols)
            {
                equipeVencedora = equipeFirst;
            }
            else if (equipeLast.Gols == equipeFirst.Gols)
            {
                // Criterio de desempate em ação

                int valorFirst;
                int valorLast;

                try
                {
                    valorFirst = Int32.Parse(equipeFirst.Sigla.Remove(0, 3));
                }
                catch (FormatException e)
                {
                    valorFirst = 0;
                }

                try
                {
                    valorLast = Int32.Parse(equipeLast.Sigla.Remove(0, 3));
                }
                catch (FormatException e)
                {
                    valorLast = 0;
                }

                equipeVencedora = (valorFirst > valorLast) ? equipeLast : equipeFirst;

            }
            else
            {
                equipeVencedora = equipeLast;
            }

            return equipeVencedora;
        }
    }
}
