﻿using System;
using System.Collections.Generic;
using squadra.Data.Models;

namespace squadra.Util
{
    public class EquipeSort : IComparer<Equipe>
    {
        public int Compare(Equipe equipeA, Equipe equipeB)
        {
            var posicaoA = Int32.Parse(equipeA.Sigla.Remove(0, 3));
            var posicaoB = Int32.Parse(equipeB.Sigla.Remove(0, 3));

            if (posicaoA > posicaoB)
                return 1;
            if (posicaoA < posicaoB)
                return -1;
            else
                return 0;
        }
    }
}
