﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace squadra.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Campeonatos",
                columns: table => new
                {
                    CampeonatoId = table.Column<Guid>(nullable: false),
                    Nome = table.Column<string>(nullable: true),
                    Finalizado = table.Column<bool>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Campeonatos", x => x.CampeonatoId);
                });

            migrationBuilder.CreateTable(
                name: "Equipes",
                columns: table => new
                {
                    EquipeId = table.Column<Guid>(nullable: false),
                    Nome = table.Column<string>(nullable: true),
                    Sigla = table.Column<string>(nullable: true),
                    Gols = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Equipes", x => x.EquipeId);
                });

            migrationBuilder.CreateTable(
                name: "Jogos",
                columns: table => new
                {
                    JogoId = table.Column<Guid>(nullable: false),
                    CampeonatoId = table.Column<Guid>(nullable: false),
                    EquipeFirst = table.Column<string>(nullable: true),
                    EquipeLast = table.Column<string>(nullable: true),
                    EquipeVencedora = table.Column<string>(nullable: true),
                    Fase = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Jogos", x => x.JogoId);
                    table.ForeignKey(
                        name: "FK_Jogos_Campeonatos_CampeonatoId",
                        column: x => x.CampeonatoId,
                        principalTable: "Campeonatos",
                        principalColumn: "CampeonatoId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CampeonatoEquipe",
                columns: table => new
                {
                    CampeonatoId = table.Column<Guid>(nullable: false),
                    EquipeId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CampeonatoEquipe", x => new { x.CampeonatoId, x.EquipeId });
                    table.ForeignKey(
                        name: "FK_CampeonatoEquipe_Equipes_CampeonatoId",
                        column: x => x.CampeonatoId,
                        principalTable: "Equipes",
                        principalColumn: "EquipeId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CampeonatoEquipe_Campeonatos_EquipeId",
                        column: x => x.EquipeId,
                        principalTable: "Campeonatos",
                        principalColumn: "CampeonatoId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Equipes",
                columns: new[] { "EquipeId", "Gols", "Nome", "Sigla" },
                values: new object[,]
                {
                    { new Guid("df457de4-367b-4f38-966e-11e02cf60b73"), 3, "Equipe 1", "EQP1" },
                    { new Guid("c9c54943-be01-48c8-8933-c319cf12af0b"), 1, "Equipe 2", "EQP2" },
                    { new Guid("ea1b87ba-4568-4f72-b6bf-ae08ba086ba8"), 0, "Equipe 3", "EQP3" },
                    { new Guid("0572b416-5af2-40e3-b526-d6c4044388a4"), 5, "Equipe 4", "EQP4" },
                    { new Guid("45907155-49ae-48f1-83fa-14cce15f05b8"), 10, "Equipe 5", "EQP5" },
                    { new Guid("30c0281b-b0e6-43a8-9445-ca23f21c66b9"), 7, "Equipe 6", "EQP6" },
                    { new Guid("e7d328b0-2bb9-4f7f-8e90-9b66ce078176"), 9, "Equipe 7", "EQP7" },
                    { new Guid("e8ff1b6f-70ff-48f6-8379-04b33b6d2249"), 8, "Equipe 8", "EQP8" },
                    { new Guid("77e7ad35-bdcd-4aed-b332-4b50a3c2dd2f"), 5, "Equipe 9", "EQP9" },
                    { new Guid("8239e31d-171a-4e3d-8608-8266a5e2d691"), 4, "Equipe 10", "EQP10" },
                    { new Guid("fade9dd5-46bf-4801-8e86-4b53ca408ced"), 6, "Equipe 11", "EQP11" },
                    { new Guid("f4329b39-e613-4e18-94a8-279788cd2493"), 0, "Equipe 12", "EQP12" },
                    { new Guid("5c056318-193c-4f88-80f6-1c0e30adbaac"), 1, "Equipe 13", "EQP13" },
                    { new Guid("24bfc89b-f174-4e60-8eea-3054a3da4f51"), 3, "Equipe 14", "EQP14" },
                    { new Guid("c860b2fe-b3e7-461f-afd9-25b5ce1e0dfa"), 2, "Equipe 15", "EQP15" },
                    { new Guid("098e1eca-2de8-42d5-b142-40859f9d9e19"), 7, "Equipe 16", "EQP16" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CampeonatoEquipe_EquipeId",
                table: "CampeonatoEquipe",
                column: "EquipeId");

            migrationBuilder.CreateIndex(
                name: "IX_Jogos_CampeonatoId",
                table: "Jogos",
                column: "CampeonatoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CampeonatoEquipe");

            migrationBuilder.DropTable(
                name: "Jogos");

            migrationBuilder.DropTable(
                name: "Equipes");

            migrationBuilder.DropTable(
                name: "Campeonatos");
        }
    }
}
