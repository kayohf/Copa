﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace squadra.Data.Models
{
    public class Equipe
    {
        public Equipe()
        {
        }

        [Key]
        public Guid EquipeId { get; set; }

        public string Nome { get; set; }

        public string Sigla { get; set; }

        public int Gols { get; set; }

        // Relacionamentos
        public List<CampeonatoEquipe> Campeonatos { get; set; }
    }
}
