﻿using System;
using System.ComponentModel.DataAnnotations;

namespace squadra.Data.Models
{
    public class Jogo
    {
        [Key]
        public Guid JogoId { get; set; }

        public Guid CampeonatoId { get; set; }

        public string EquipeFirst { get; set; }

        public string EquipeLast { get; set; }

        public string EquipeVencedora { get; set; }

        public int Fase { get; set; }

        // Relacionamentos
        public Campeonato Campeonato { get; set; }
    }
}
