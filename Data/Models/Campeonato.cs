﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace squadra.Data.Models
{
    public class Campeonato
    {
        public Campeonato()
        {
        }

        [Key]
        public Guid CampeonatoId { get; set; }

        public string Nome { get; set; }

        public Boolean Finalizado { get; set; }

        public DateTime CreatedAt { get; set; }

        // Relacionamentos
        public virtual List<CampeonatoEquipe> Equipes { get; set; }
    }
}
