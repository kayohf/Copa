﻿using System;

namespace squadra.Data.Models
{
    public class CampeonatoEquipe
    {
        public Guid CampeonatoId { get; set; }
        public Campeonato Campeonato { get; set; }

        public Guid EquipeId { get; set; }
        public Equipe Equipe { get; set; }
    }
}
