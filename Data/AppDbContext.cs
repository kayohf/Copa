﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using squadra.Data.Models;

namespace squadra.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext() : base()
        {
        }

        public AppDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<CampeonatoEquipe>()
            .HasKey(ce => new { ce.CampeonatoId, ce.EquipeId });

            modelBuilder.Entity<CampeonatoEquipe>()
                .HasOne(ce => ce.Campeonato)
                .WithMany(e => e.Equipes)
                .HasForeignKey(ce => ce.EquipeId);

            modelBuilder.Entity<CampeonatoEquipe>()
                .HasOne(ce => ce.Equipe)
                .WithMany(c => c.Campeonatos)
                .HasForeignKey(ce => ce.CampeonatoId);

            modelBuilder.Entity<Equipe>().ToTable("Equipes");

            modelBuilder.Entity<Campeonato>().ToTable("Campeonatos");

            modelBuilder.Entity<Jogo>().ToTable("Jogos");

            modelBuilder.Entity<Equipe>()
                .HasData(
                    new Equipe
                    {
                        EquipeId = Guid.NewGuid(),
                        Nome = "Equipe 1",
                        Sigla = "EQP1",
                        Gols = 3
                    },
                    new Equipe
                    {
                        EquipeId = Guid.NewGuid(),
                        Nome = "Equipe 2",
                        Sigla = "EQP2",
                        Gols = 1
                    },
                    new Equipe
                    {
                        EquipeId = Guid.NewGuid(),
                        Nome = "Equipe 3",
                        Sigla = "EQP3",
                        Gols = 0
                    },
                    new Equipe
                    {
                        EquipeId = Guid.NewGuid(),
                        Nome = "Equipe 4",
                        Sigla = "EQP4",
                        Gols = 5
                    },
                    new Equipe
                    {
                        EquipeId = Guid.NewGuid(),
                        Nome = "Equipe 5",
                        Sigla = "EQP5",
                        Gols = 10
                    },
                    new Equipe
                    {
                        EquipeId = Guid.NewGuid(),
                        Nome = "Equipe 6",
                        Sigla = "EQP6",
                        Gols = 7
                    },
                    new Equipe
                    {
                        EquipeId = Guid.NewGuid(),
                        Nome = "Equipe 7",
                        Sigla = "EQP7",
                        Gols = 9
                    },
                    new Equipe
                    {
                        EquipeId = Guid.NewGuid(),
                        Nome = "Equipe 8",
                        Sigla = "EQP8",
                        Gols = 8
                    },
                    new Equipe
                    {
                        EquipeId = Guid.NewGuid(),
                        Nome = "Equipe 9",
                        Sigla = "EQP9",
                        Gols = 5
                    },
                    new Equipe
                    {
                        EquipeId = Guid.NewGuid(),
                        Nome = "Equipe 10",
                        Sigla = "EQP10",
                        Gols = 4
                    },
                    new Equipe
                    {
                        EquipeId = Guid.NewGuid(),
                        Nome = "Equipe 11",
                        Sigla = "EQP11",
                        Gols = 6
                    },
                    new Equipe
                    {
                        EquipeId = Guid.NewGuid(),
                        Nome = "Equipe 12",
                        Sigla = "EQP12",
                        Gols = 0
                    },
                    new Equipe
                    {
                        EquipeId = Guid.NewGuid(),
                        Nome = "Equipe 13",
                        Sigla = "EQP13",
                        Gols = 1
                    },
                    new Equipe
                    {
                        EquipeId = Guid.NewGuid(),
                        Nome = "Equipe 14",
                        Sigla = "EQP14",
                        Gols = 3
                    },
                    new Equipe
                    {
                        EquipeId = Guid.NewGuid(),
                        Nome = "Equipe 15",
                        Sigla = "EQP15",
                        Gols = 2
                    },
                    new Equipe
                    {
                        EquipeId = Guid.NewGuid(),
                        Nome = "Equipe 16",
                        Sigla = "EQP16",
                        Gols = 7
                    }
                );
        }

        public DbSet<Equipe> Equipes { get; set; }
        public DbSet<Campeonato> Campeonatos { get; set; }
        public DbSet<Jogo> Jogos { get; set; }
    }
}
