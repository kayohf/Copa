using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using squadra.Data;
using squadra.Data.Models;
using squadra.Services;
using squadra.Util;

namespace squadra.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CampeonatosController : ControllerBase
    {
        private readonly AppDbContext _context;

        public CampeonatosController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Campeonatos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Campeonato>>> GetCampeonatos()
        {
            return await _context.Campeonatos.ToListAsync();
        }

        // GET: api/Campeonatos/5/Jogos
        [HttpGet]
        [Route("{id}/Jogos")]
        public async Task<ActionResult<IEnumerable<Jogo>>> GetJogos(Guid id)
        {
            List<Jogo> jogos = await _context
                .Jogos
                .Where(j => j.CampeonatoId == id)
                .OrderByDescending(j => j.Fase)
                .ToListAsync();

            if (jogos == null)
            {
                return NotFound();
            }

            return jogos;
        }

        // GET: api/Campeonatos/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Campeonato>> GetCampeonato(Guid id)
        {
            var campeonato = await _context.Campeonatos.Where(c => c.CampeonatoId == id).FirstOrDefaultAsync();

            if (campeonato == null)
            {
                return NotFound();
            }

            return campeonato;
        }

        // POST: api/Campeonatos
        [HttpPost]
        public async Task<IActionResult> PostCampeonato([FromForm] string nome, [FromForm] string selecao)
        {
            // Valida se ao menos a selecao está vindo
            if (selecao == null)
            {
                return new JsonResult(new
                {
                    sucesso = false,
                    mensagem = "Você deve selecionar 08 equipes"
                });
            }

            string[] selecaoIds = selecao.Split(",");

            // Valida se a seleção é igual a 8 itens
            if (selecaoIds.Length != 8)
            {
                return new JsonResult(new
                {
                    sucesso = false,
                    mensagem = "Você deve selecionar 08 equipes"
                });
            }

            // Agora vamos pegar todas as Equipes selecionadas
            // e ordenar
            List<Equipe> equipes = new List<Equipe>();

            foreach (string idEquipe in selecaoIds)
            {
                // Verifica se a entrada é um Guid válido
                if (!Guid.TryParse(idEquipe, out var equipeGuid))
                {
                    return new JsonResult(new
                    {
                        sucesso = false,
                        mensagem = "Um ou mais valores enviados não são válidos"
                    });
                }

                equipes.Add(_context.Equipes.Find(equipeGuid));
            }

            equipes.Sort(new EquipeSort());

            Campeonato campeonato = new Campeonato();

            campeonato.CampeonatoId = new Guid();
            campeonato.Nome = (nome != "undefined") ? nome : "Não Identificado";
            campeonato.Finalizado = true;
            campeonato.CreatedAt = DateTime.Now;
            campeonato.Equipes = new List<CampeonatoEquipe>();

            foreach (string idEquipe in selecaoIds)
            {
                Equipe selecionado = _context.Equipes.Find(Guid.Parse(idEquipe));

                campeonato.Equipes.Add(new CampeonatoEquipe
                {
                    Campeonato = campeonato,
                    Equipe = selecionado
                });
            }

            _context.Campeonatos.Add(campeonato);

            await _context.SaveChangesAsync();

            // Vamos fazer os jogos agora e salvar ligando ao
            // campeonato
            var processarJogos = new ProcessarJogosService(equipes);

            processarJogos.gerarJogos();

            List<Jogo> jogos = processarJogos.resultado();

            foreach (Jogo jogo in jogos)
            {
                jogo.Campeonato = campeonato;

                _context.Jogos.Add(jogo);
            }

            await _context.SaveChangesAsync();

            return new JsonResult(new {
                sucesso = true,
                campeonatoId = campeonato.CampeonatoId,
                mensagem = "Campeonato criado com Sucesso"
            });
        }

    }
}
